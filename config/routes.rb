Rails.application.routes.draw do
  root to: 'home#index'
  get '/docs/index', to: 'docs#index'
  # 这里无法使用namespace https://github.com/doorkeeper-gem/doorkeeper/wiki/Customizing-routes
  scope :api do
    use_doorkeeper scope: 'oauth2'
  end
  namespace :api do
    namespace :v1 do
      get 'me', controller: 'credentials'
      resources :users, only: [:create] do
        post 'send_code', on: :collection
      end
    end
  end
end
