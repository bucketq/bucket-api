module Bucket
  SMS_PROVIDER        = ENV['SMS_PROVIDER'].freeze
  SMS_LIMIT_PER_IP    = ENV['SMS_LIMIT_PER_IP'].to_i.freeze

  # 有数据验证错误，且需要显示 message
  class ValidationError < StandardError
  end

  # 有数据验证错误，但是不用来显示 message
  class InvalidParamsError < StandardError
  end

  class HTTPError < StandardError
  end

  class APIBlockedError < StandardError
  end
end
