Backburner.configure do |config|
  config.beanstalk_url    = ENV['BEANSTALKD_SERVERS'].split(',')
  config.tube_namespace   = ENV['BEANSTALKD_NAMESPACE']
  config.on_error         = lambda { |e, name, args, job| Bugsnag.notify(e, data: { backburner: {name: name, args: args, job: job} }) }
  config.max_job_retries  = 3  # default 0 retries
  config.retry_delay      = 10 # default 5 seconds
  config.default_priority = 100
  config.respond_timeout  = 1800
  config.default_worker   = Backburner::Workers::Simple
  config.logger           = Logger.new("#{Rails.root}/log/backburner_#{ENV['BEANSTALKD_NAMESPACE']}_#{Rails.env}.log")
  config.primary_queue    = 'default'
  config.priority_labels  = Backburner::Configuration::PRIORITY_LABELS.merge(useless: 1000)  # {high: 0, medium: 100, low: 200}
  config.reserve_timeout  = nil
end
