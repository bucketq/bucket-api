module Bucket
  def self.redis(&block)
    raise ArgumentError, 'requires a block' unless block
    redis_pool.with(&block)
  end

  def self.redis_pool
    @redis_pool ||= ConnectionPool.new(size: ENV['REDIS_POOL_SIZE'].to_i) { Redis.new(url: ENV['REDIS_URL']) }
  end
end
