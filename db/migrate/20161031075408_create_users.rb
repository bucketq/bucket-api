class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :name,               limit: 64
      t.string :nickname,           limit: 64
      t.string :email,              limit: 64
      t.string :mobile,             limit: 32, null: false
      t.string :password_digest,    limit: 64
      t.string :intr,               limit: 1000
      t.string :source,             limit: 64
      t.integer :gender,            limit: 4
      t.datetime :birthday
      t.integer :failed_attempts,   deafult: 0
      t.datetime :locked_at

      t.timestamps
    end
  end
end
