class CreateCrawls < ActiveRecord::Migration[5.0]
  def change
    create_table :crawls do |t|
      t.integer :app_id
      t.integer :status
      t.string :memo

      t.timestamps
    end
  end
end
