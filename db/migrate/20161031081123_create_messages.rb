class CreateMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :messages do |t|
      t.string :title,               limit: 100, null: false
      t.string :body,                limit: 10000, null: false
      t.integer :receiver_id,        null: false
      t.integer :status,             limit: 4

      t.timestamps
    end
  end
end
