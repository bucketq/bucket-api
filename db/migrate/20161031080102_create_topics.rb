class CreateTopics < ActiveRecord::Migration[5.0]
  def change
    create_table :topics do |t|
      t.integer :app_id,        null: false
      t.integer :enabled,       limit: 4
      t.text :intr,             limit: 2000
      t.datetime :beg_at
      t.datetime :end_at

      t.timestamps
    end
  end
end
