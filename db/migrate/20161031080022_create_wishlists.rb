class CreateWishlists < ActiveRecord::Migration[5.0]
  def change
    create_table :wishlists do |t|
      t.integer :app_id,        null: false
      t.integer :user_id,       null: false
      t.integer :status,        limit: 4
      t.string :memo,           limit: 2000

      t.timestamps
    end
  end
end
