class CreateAdmins < ActiveRecord::Migration[5.0]
  def change
    create_table :admins do |t|
      t.string :name,                 limit: 32
      t.string :email,                limit: 64
      t.string :mobile,               limit: 64
      t.string :password_digest,      limit: 200, null: false
      t.string :q_key,                limit: 100
      t.string :q_bucket,             limit: 100
      t.string :intr,                 limit: 1000

      t.timestamps
    end
  end
end
