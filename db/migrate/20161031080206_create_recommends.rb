class CreateRecommends < ActiveRecord::Migration[5.0]
  def change
    create_table :recommends do |t|
      t.string :title,      limit: 100
      t.string :intr,       limit: 2000
      t.string :seq,        limit: 32
      t.integer :enabled,   limit: 4

      t.timestamps
    end
  end
end
