class CreateImages < ActiveRecord::Migration[5.0]
  def change
    create_table :images do |t|
      t.integer :app_id
      t.string :title
      t.string :q_key
      t.string :q_bucket
      t.integer :seq

      t.timestamps
    end
  end
end
