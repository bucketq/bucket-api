class CreateArtists < ActiveRecord::Migration[5.0]
  def change
    create_table :artists do |t|
      t.string :name
      t.integer :track_id
      t.string :view_url

      t.timestamps
    end
  end
end
