class CreateLikes < ActiveRecord::Migration[5.0]
  def change
    create_table :likes do |t|
      t.integer :likeable_id,       null: false
      t.string :likeable_type,      limit: 64, null: false
      t.integer :user_id

      t.timestamps
    end
  end
end
