class CreateGenres < ActiveRecord::Migration[5.0]
  def change
    create_table :genres do |t|
      t.string :name
      t.string :ename
      t.integer :track_id
      t.string :view_url
      t.integer :status

      t.timestamps
    end
  end
end
