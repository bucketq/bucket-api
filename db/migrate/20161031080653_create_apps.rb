class CreateApps < ActiveRecord::Migration[5.0]
  def change
    create_table :apps do |t|
      # 基本信息
      t.string :track_name,
      t.integer :track_id,
      t.string :track_view_url
      # App 图案
      t.string :q_artwork_url_512
      t.string :q_artwork_url_60
      t.string :q_artwork_url_100
      t.string :q_bucket
      # 版本信息
      t.string :release_notes
      t.string :version
      t.string :description
      t.string :minimum_os_version
      t.string :language_codes_iso
      t.datetime :release_date
      t.datetime :current_version_release_date
      # App图片，需要迁移到 images 表
      t.string :screenshot_urls
      # 关联的类别
      t.string :genres
      t.string :genre_ids
      t.string :primary_genre_id
      t.string :primary_genre_name
      # 作者信息，数据需要迁移到 artists 表
      t.integer :artist_id
      t.string :artist_name
      t.string :artist_view_url
      # 价格
      t.integer :price
      t.string :formatted_price
      #
      t.integer :status
      t.integer :flag
      # 字母
      t.integer :letter_id
      # 评论数目与评分
      t.integer :user_rating_count
      t.integer :average_user_rating
      t.integer :local_average_user_rating
      t.integer :local_user_rating_count
      # 数据统计
      t.integer :lick_count
      t.integer :download_count
      t.integer :wish_count

      t.timestamps
    end
  end
end
