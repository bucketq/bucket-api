class CreateFeedbacks < ActiveRecord::Migration[5.0]
  def change
    create_table :feedbacks do |t|
      t.integer :user_id
      t.integer :status,          limit: 4
      t.string :memo,             limit: 2000
      # 处理时间
      t.datetime :processed_date
      t.timestamps
    end
  end
end
