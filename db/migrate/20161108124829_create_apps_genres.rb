class CreateAppsGenres < ActiveRecord::Migration[5.0]
  def change
    create_table :apps_genres do |t|
      t.integer :app_id
      t.integer :genre_id

      t.timestamps
    end
  end
end
