class CreateComments < ActiveRecord::Migration[5.0]
  def change
    create_table :comments do |t|
      t.integer :user_id
      t.integer :app_id,         null: false
      t.string :memo,            limit: 2000
      t.integer :status,         limit: 4
      t.integer :rate,           limit: 4

      t.timestamps
    end
  end
end
