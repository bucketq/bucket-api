class SmsSendJob < ActiveJob::Base
  queue_as :sms

  def perform(*args)
    mobile, message, provider = args

    case provider
    when 'luosimao'
      result  = ZBJ::Service::Provider::Luosimao.send_message(mobile, message)
      error, msg = result['error'], result['msg']
    end
  end
end
