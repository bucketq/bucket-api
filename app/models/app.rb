class App < ApplicationRecord
  belongs_to :letter
  # # 作者
  # belongs_to :artist
  # 类型
  belongs_to :apps_genre
  has_many :genres, through: :apps_genre
  # # 推荐分组
  # belongs_to :recommend
  # # 图片
  # has_many :images
  # # 评论
  # has_many :comments
  # # 推荐文章
  # has_one :topic
  # # 心愿单
  # has_many :wishlists
end
