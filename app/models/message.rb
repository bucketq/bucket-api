class Message < ApplicationRecord
  belongs_to :receiver, foreign_key: 'receiver_id', class_name: 'User'
end
