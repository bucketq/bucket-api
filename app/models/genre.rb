class Genre < ApplicationRecord
  belongs_to :apps_genre
  has_many :apps, through: :apps_genre
end
