class AppsGenre < ApplicationRecord
  has_many :apps
  has_many :genres
end
