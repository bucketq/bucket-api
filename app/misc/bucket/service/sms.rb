module Bucket
  module Service
    class Sms
      MESSAGE_SIGNATURE = '【Bucket】'.freeze

      def self.send_message(mobile, message, provider=ENV['SMS_PROVIDER'])
        SmsSendJob.perform_later mobile, "#{message}#{MESSAGE_SIGNATURE}", provider
      end
    end
  end
end
