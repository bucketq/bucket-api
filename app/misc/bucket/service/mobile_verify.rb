module Bucket
  module Service
    class MobileVerify
      class RateLimitError < ::StandardError; end

      def initialize(mobile, remote_ip)
        @mobile   = mobile
        @ttl      = 10 * 60  # 验证码有效时间 10分钟
        @interval = 1  * 60  # 每次发送的间隔时间 1分钟
        @code_redis_key  = "mobile_verify_service:code:#{@mobile}"
        @limit_redis_key = "mobile_verify_service:limit:#{remote_ip}"
      end

      # 发送验证码
      # 如果在 @interval 时间内继续调用，则 RateLimitError
      def send_verify_code
        raise RateLimitError, '请一分钟后再试' unless sendable?

        code = Random.new.rand(1000..9999).to_s

        Bucket.redis do |redis|
          redis.multi
          # code: 验证码； verified_times: 已被验证的次数
          redis.hmset @code_redis_key, ['code', code, 'verified_times', 0]
          redis.expire @code_redis_key, @ttl
          redis.incr @limit_redis_key
          redis.exec
          redis.expire(@limit_redis_key, @interval) if redis.ttl(@limit_redis_key) == -1
        end

        Sms.send_message @mobile, "尊敬的用户，您的验证码为：#{code}，请输入进行验证。"
      end

      # 验证是否匹配
      def valid_code?(code)
        saved_code, verified_times = Bucket.redis { |redis| redis.hmget @code_redis_key, 'code', 'verified_times' }
        return false if saved_code.nil? || verified_times.nil?
        raise RateLimitError, '多次验证失败，请重新获取验证码' if verified_times.to_i > 10  # 尝试验证超过10次就异常失败
        Bucket.redis { |redis| redis.hincrby @code_redis_key, 'verified_times', 1 }
        Rack::Utils.secure_compare saved_code, code
      end

      # 删除验证码
      def delete_code!
        Bucket.redis do |redis|
          redis.del @code_redis_key
        end
      end

      private

      # 是否可以发送
      def sendable?
        # 检查单位时间单个IP发送数量
        ip_send_times = Bucket.redis { |redis| redis.get @limit_redis_key }
        return false if ip_send_times.to_i > Bucket::SMS_LIMIT_PER_IP

        # 检查单位时间单个手机号的发送间隔时间
        ttl = Bucket.redis { |redis| redis.ttl @code_redis_key }
        return true if (ttl == -2) || (ttl == -1)  # Starting with Redis 2.8 returns -2 if the key does not exist
        @ttl - ttl > @interval
      end
    end
  end
end
