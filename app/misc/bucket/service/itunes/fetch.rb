# Job
# 0 是否可以先获取 Letter Genre 的track信息

# 1 获取所有App id，Letter id, Genre id,
# Genre:
# doc.xpath("//div[@id='genre-nav']//ul/li/a")[1].attributes['href'].value
# doc = Nokogiri::HTML(open("https://itunes.apple.com/cn/genre/mac-shang-wu/id12001?mt=12/"))
# doc.xpath("//div[@id='genre-nav']//ul/li/a").each do |item|
#   Genre.create(name: item.children.text, view_url: item.attributes['href'].value)
# end
# Letter:
# doc.xpath("//div[@id='selectedgenre']/ul/li/a").each do |item|
#   Letter.create(name: item.children.text)
# end
#  App
# Genre.all.each do |gen|
#   Letter.all.each do |let|
#     doc = Nokogiri::HTML(open("#{gen.view_url}&letter=#{let.name}"))
#     pages = doc.xpath("//ul[@class='list paginate']")
#     if pages.blank?
#       # 不存在分页，然后直接抓取App
#       doc.xpath("//div[@id='selectedcontent']//li/a").each do |item|
#         let.apps.create(track_view_url: item.attributes['href'], track_name: item.children.text, track_id: item.attributes['href'].value[/\d+/].to_i)
#       end
#     else
#       pages.children do |page|
#         # 第一页还是doc
#         doc = Nokogiri::HTML(open(page.children.first.attributes['href'].value))
#         doc.xpath("//div[@id='selectedcontent']//li/a").each do |item|
#           let.apps.create(track_view_url: item.attributes['href'], track_name: item.children.text, track_id: item.attributes['href'].value[/\d+/].to_i)
#         end
#       end
#       # 有分页，循环分页内容
#     end
#   end
# end
# 2 现有的数据库App Track Id 获取信息，是否变更, 标记「新应用、已上线、下线」状态，根据价格变化标记：「限免、减价、涨价」，做好日志历史记录跟踪。
# App
a1 = App.first
resp = Net::HTTP.get_response(URI.parse("https://itunes.apple.com/cn/lookup?id=#{a1.track_id}"))
if resp.code == '200'
  buffer = JSON.parse(resp.body)
  if buffer['resultCount'] == 1
    buffer['results'].each do |item|
      current_app = App.find_by(track_id: item['trackId'])
      if current_app
        current_app.update_attributes({
          q_artwork_url_512: item['artworkUrl512'],
          q_artwork_url_60: item['artworkUrl60'],
          q_artwork_url_100: item['artworkUrl60'],
          release_notes: item['releaseNotes'],
          version: item['version'],
          description: item['description'],
          minimum_os_version: item['minimumOsVersion'],
          language_codes_iso: item['languageCodesISO2A'],
          release_date: item['releaseDate'],
          current_version_release_date: item['currentVersionReleaseDate'],
          screenshot_urls: item['screenshotUrls'],
          primary_genre_id: item['primaryGenreId'],
          primary_genre_name: item['primaryGenreName'],
          artist_id: item['artistId'],
          artist_name: item['artistName'],
          artist_view_url: item['artistViewUrl'],
          price: item['price'],
          formatted_price: item['formattedPrice'],
          user_rating_count: item['userRatingCount'],
          average_user_rating: item['averageUserRating']})
      end
    end
  end
end
# genre_ids
# genres
# currency "CNY"
# contentAdvisoryRating "4+"
# source: 'cask' || 'store'
# link_id: 链接cask的App id
# sha256:
# why not build new table. casks
# Cask
# 3 后台处理一个任务，单独抓取一个App id 的数据。
