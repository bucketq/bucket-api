class Api::V1::CredentialsController < Api::V1::BaseController
  before_action :doorkeeper_authorize!

  def me
    @user = current_resource_owner
  end

end
