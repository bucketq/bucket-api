class Api::V1::BaseController < ApplicationController
  # disable the CSRF token
  protect_from_forgery with: :null_session

  helper_method :current_resource_owner

  # Find the user that owns the access token
  def current_resource_owner
    ::User.find(doorkeeper_token.resource_owner_id) if doorkeeper_token
  end
  
  # 渲染 JSON  消息
  def render_message(message, status, other_messages={})
    render json: { message: message }.merge!(other_messages), status: status
  end

  # 渲染 JSON errors 消息
  def render_error_messages(errors, message=nil)
    message = message || errors.full_messages.first || '数据校验错误'
    render_message message, 422, errors: errors
  end
end
