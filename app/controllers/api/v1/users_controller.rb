class Api::V1::UsersController < Api::V1::BaseController

  def create
    # 校验手机验证码
    smv = ::Bucket::Service::MobileVerify.new(params[:mobile], request.remote_ip)
    unless smv.valid_code?(params[:code])
      render_message '验证码不正确，请重新输入', 422
      return
    end

    # 如果手机号在系统中存在直接登录，否则创建新帐号并登录
    if user = ::User.find_by(mobile: params[:mobile])
      # 存在用户的逻辑
      new_member = false
    else
      # 新用户的逻辑
      ActiveRecord::Base.transaction do
        user = ::User.new(nickname: params[:mobile], mobile: params[:mobile])
        user.save!(validate: false)
      end
      new_member = true
    end

    # TODO 对于通过手机号直接登录用户，需要校验是否被锁定
    # 直接返回 token 自动登录
    access_token = generate_token_for(user).body
    status       = access_token[:state]
    render json: access_token.merge(new_member: new_member).to_json, status: status
  end

  def send_code
    mobile = request.request_parameters[:mobile]
    send_verify_code(mobile) do
    end
  end


  private

  def send_verify_code(mobile)
    if /\A1\d{10}\z/ =~ mobile  # NOTE: International ?
      mv_service = ::Bucket::Service::MobileVerify.new(mobile, request.remote_ip)
      yield
      mv_service.send_verify_code
      head :no_content
    else
      render_message '请输入正确的手机号码', 422
    end
  end

  # 为登录用户生成access_token
  def generate_token_for(user)
    door_server = ::Doorkeeper::Server.new(self)
    credentials = door_server.credentials
    client = ::Doorkeeper::Application.by_uid_and_secret credentials.uid, credentials.secret if credentials
    if client
      server = Doorkeeper.configuration
      scopes ||= server.default_scopes
      access_token = ::Doorkeeper::AccessToken.find_or_create_for(client, user.id, scopes,
                                                    ::Doorkeeper::OAuth::Authorization::Token.access_token_expires_in(server, client),
                                                    server.refresh_token_enabled?)
      ActiveRecord::Base.transaction do
        # 返回登录access_token
        ::Doorkeeper::OAuth::TokenResponse.new(access_token)
      end
    else
      ::Doorkeeper::OAuth::ErrorResponse.new(name: 'invalid_client', state: 401)
    end
  end
end
