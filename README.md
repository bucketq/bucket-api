### Bucket

#### 依赖
* Ruby 2.3.1, 推荐使用[rbenv](https://github.com/rbenv/rbenv)管理Ruby版本
* 配置 `bundler` 镜像地址, `bundle config mirror.https://rubygems.org https://gems.ruby-china.org`

#### 准备工作

  目前可以通过 [foreman](https://github.com/ddollar/foreman) 管理一部分进程，参见 `Procfile`。开发的过程中 `foreman start` 启动依赖服务，尤其需要登录的时候。

  1. `Foreman`       => `brew install foreman`
  2. `MySQL`         => `brew install mysql`
  3. `Redis`         => `brew install redis`
  4. `beanstalkd`    => `brew install beanstalkd` (注意: 生产环境一定要带 `-b` 选项启动)


#### 配置文件
  1. `config/database.yml`
  2. `config/application.yml`

#### 启动
  1. `foreman start`
  2. `rails s -b 0.0.0.0`

#### API 文档
  1. 访问 `http://localhost:3000/docs/index`
  2. 帐号: `bucket`, 密码: `123123123`
